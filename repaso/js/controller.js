/**
*  Module
*
* Description
*/
angular.module('Repaso', ['ngRoute', 'ui.bootstrap'])
.config(function($routeProvider) {
  $routeProvider.when('/posts', {
    controller: 'PostsController',
    templateUrl: 'post-list.html'
  });
  $routeProvider.when('/post/:id', {
    controller: 'PostController',
    templateUrl: 'post-details.html'
  });
  $routeProvider.otherwise('/posts');
})
.service('Alerts', function($rootScope) {
  $rootScope.alerts = [];
  this.addAlert = function(type, msg) {
    $rootScope.alerts.push(
        {
          type: type,
          msg: msg
        }
      );
  };
})
.controller('AppCtrl', function($scope, $http, Alerts) {
  $scope.alerts = Alerts.alerts;
})
.controller('PostsController', function($scope, $http, Alerts) {
  $scope.posts = [];
  $http.get('http://jsonplaceholder.typicode.com/posts')
    .then(function(data) {
      // success handler
      $scope.posts = data.data;
      console.log(data);
    }, function(msg, status, headers, config) {
      // Error handler
      Alerts.addAlert('danger', 'Status: ' + msg.status + '<br>Mensaje: ' + msg.data );

      console.error(msg.data);
    });
})
.controller('PostController', function($scope, $http, $routeParams, $modal) {
  $scope.title = 'Editar Post';
  $scope.post = {};
  $http.get('http://jsonplaceholder.typicode.com/posts/'+$routeParams.id)
    .then(function(data) {
      // success handler
      $scope.post = data.data;
      console.log(data);
    }, function(msg) {
      // Error handler
      $scope.alerts.push(
        {
          type: 'danger',
          msg: 'Status: ' + msg.status + '<br>Mensaje: ' + msg.data
        }
      );
      console.error(msg);
    });
  $scope.comments = [];
  $http.get('http://jsonplaceholder.typicode.com/posts/'+$routeParams.id + '/comments')
    .success(function(data, status, headers, config) {
      $scope.comments = data;
      console.log(headers);
    })
    .error(function(msg) {
      console.error(msg);
    });

  $scope.edit = function(post) {

    var modalInstance = $modal.open({
      animation: $scope.animationsEnabled,
      templateUrl: 'edit-post.html',
      resolve: {
        items: function () {
          return $scope.items;
        }
      }
    });

    modalInstance.result.then(function (selectedItem) {
      $scope.selected = selectedItem;
    }, function () {
      $log.info('Modal dismissed at: ' + new Date());
    });

  }
})
.controller('AppCtrl', function($scope){

})
