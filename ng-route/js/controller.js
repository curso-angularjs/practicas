var app = angular.module("CursoAngular", ["ngRoute"]);


app.run(["$rootScope", function(rs) {
  rs.msg = "Mensaje Padre";
}]);

app.config(['$routeProvider',function($routeProvider) {
  $routeProvider.when('/', {
    controller: 'PostsList',
    templateUrl: 'templates/post-list.html'
  });
  $routeProvider.when('/post/:id', {
    controller: 'PostDetails',
    templateUrl: 'templates/post-details.html'
  })
  $routeProvider.otherwise('/');

}]);

app.controller('MiControlador', ['$scope', 'localstorage', 'testService', function($scope, localstorage, testService) {

  var todo =  [];
  $scope.nuevaActividad = {};
  $scope.todo = todo;

  $scope.agregar = function() {
    todo.push(angular.copy($scope.nuevaActividad));
    $scope.nuevaActividad = {};
    localstorage.setObject('todo-list', todo);
  };
  $scope.eliminar = function(actividad) {
    var index = todo.indexOf(actividad);
    todo.splice(index, 1);
    console.log(todo);
    localstorage.setObject('todo-list', todo);
  }
  $scope.setToday = function() {
      $scope.nuevaActividad.fecha = new Date();
  };

  $scope.alert = function(seconds) {
    testService.delay(seconds)
      .then(function(mgs) {
        //success handle
        alert(mgs);
      }, function(msg) {
        //Error handle
        console.log(msg);
      });
  }


}]);

app.controller('PostsList', function($scope, $ws, $http ) {
  $scope.posts = [];
  $http.get('http://jsonplaceholder.typicode.com/posts')
    .then(function(data) {
      // success handler
      $scope.posts = data.data;
      console.log(data);
    }, function(msg) {
      // Error handler
      console.error(msg);
    });
});
app.controller('PostDetails', ['$scope', '$routeParams', '$http', function($scope, $routeParams, $http){
  $scope.post = {};
  $http.get('http://jsonplaceholder.typicode.com/posts/'+$routeParams.id)
    .then(function(data) {
      // success handler
      $scope.post = data.data;
      console.log(data);
    }, function(msg) {
      // Error handler
      console.error(msg);
    });

}])
