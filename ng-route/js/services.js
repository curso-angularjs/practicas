/**
*  Module
*
* Description
*/
angular.module('CursoAngular', [])

.factory('localstorage', function() {
  var localStorage = {};
  localStorage.setObject = function(key, object) {
    window.localStorage.setItem(key, JSON.stringify(object));
    return true;
  }
  localStorage.getObject = function(key, defaultv) {
    var aux = window.localStorage.getItem(key);
    if(aux) {
      return JSON.parse(aux);
    }
    return defaultv;
  }
  return localStorage;

})
.factory('testService', function($timeout, $q) {
  return {
    delay : function(seconds) {
      var dfd = $q.defer();
      $timeout(function() {
        dfd.reject('ERROR');
      }, seconds*1000);
      return dfd.promise;
    }
  }

})
.factory('$ws', function($http) {
  var $wsHandler = {

  };

  $wsHandler.getPosts = function() {
    return $http.get('http://jsonplaceholder.typicode.com/posts');
  };

  return $wsHandler;
});


