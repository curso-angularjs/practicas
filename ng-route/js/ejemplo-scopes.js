var app = angular.module("CursoAngular", []);


app.run(function($rootScope) {
  $rootScope.msg = "Mensaje Padre";
});

app.controller('MiControlador', ['$scope', '$rootScope' , function($scope, rs) {
  $scope.hasChanged = function() {
    alert('He cambiado');
  }
  $scope.styles={};
  $scope.toggleBorder = function() {
    if(!$scope.styles.border) {
      $scope.styles.border = 'solid 4px black';
    }
  }
  var msgElement = document.getElementById('mensaje');
  msgElement.addEventListener('change', function(e) {
    alert('El input ha cambiado.');
  });
 window.setTimeout(function() {
   // var msgElement = document.getElementById('mensaje');
   // msgElement.innerHtml = "Hola kelly.";
   $scope.$apply(function() {
     rs.msg = "Hola Kelly";
   });
   console.log('Msg: ' + rs.msg);
 }, 4000);
}]);

app.controller('ControladorHijo', ['$scope', function($scope){
  $scope.msg = 'Hola Angela';
}]);
