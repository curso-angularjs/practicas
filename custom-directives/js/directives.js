/**
  * MyApp.directives le
  *
  * Description
  */
angular.module('MyApp.directives', [])
  .directive('roundImage', function() {
  return {
    link: function(scope, element, attrs) {
      var url = attrs.roundImage;
      element.css({
        'background': 'url('+url+')',
        'width': '150px',
        'height': '150px',
        'background-size': 'cover',
        'background-position': 'center center',
        'border-radius': '50%'
      });
    }
  }

  })

  .directive('myAutocomplete', [function(){
    // Runs during compile
    return {
      // name: '',
      // priority: 1,
      // terminal: true,
      // scope: {}, // {} = isolate, true = child, false/undefined = no change
      // controller: function($scope, $element, $attrs, $transclude) {},
      // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
      // restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
      // template: '',
      // templateUrl: '',
      // replace: true,
      // transclude: true,
      // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
      link: function(scope, element, attrs, controller) {
        var nombres = scope.$eval(attrs.myAutocomplete);
        $(element).autocomplete({
          source: nombres,
          autoFocus: true,
          select: function(e, ui) {
            e.preventDefault();
            console.log(["change", ui.item.value]);
            if(attrs.myAutocompleteTarget) {
              scope.$apply(function(scope) {
                scope[attrs.myAutocompleteTarget] = ui.item.value;
              });
            }
            $(element).val(ui.item.value);
          },
          focus: function(e, ui) {
            e.preventDefault();
            console.log(["focus", ui.item.value]);
            if(attrs.myAutocompleteTarget) {
              scope.$apply(function(scope) {
                scope[attrs.myAutocompleteTarget] = ui.item.value;
              });
            }
          }
        });
      }
    };
  }])
.directive('myPuzzle', [function(){
  // Runs during compile
  return {
    // name: '',
    // priority: 1,
    // terminal: true,
    // scope: {}, // {} = isolate, true = child, false/undefined = no change
    // controller: function($scope, $element, $attrs, $transclude) {},
    // require: 'ngModel', // Array = multiple requires, ? = optional, ^ = check parent elements
    // restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
    // template: '',
    // templateUrl: '',
    // replace: true,
    // transclude: true,
    // compile: function(tElement, tAttrs, function transclude(function(scope, cloneLinkingFn){ return function linking(scope, elm, attrs){}})),
    link: function(scope, element, attrs, controller) {
      var src = attrs.myPuzzleSrc;

      var img = document.createElement('img');

      img.onload = function() {

        $(img).jqPuzzle();
      }

      img.src = attrs.myPuzzleSrc;
      $(element).append(img);
    }
  };
}]);

