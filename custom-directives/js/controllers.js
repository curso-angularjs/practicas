angular.module('MyApp', ['MyApp.directives'])
.controller('AppCtrl', ['$scope', function($scope){
  $scope.nombres = ["Angela Gomez", "Mario Castillo", "Kelly maría", "Gustavo peláez", "Edward Gomez", "Felipe Echeverri", "Walter Estrada"];
  $scope.seleccionarNombre = function(nombre) {
    $scope.nombreSeleccionado = nombre;
  }
}]);

